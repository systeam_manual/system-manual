## 이슈
변경 된 지역 데이터 미 반영 시 지역 단위 차단 기능이 오작동 할 수 있음  
주기적 (매 분기) 으로 업데이트 진행 필요

## 상세설명
- [F5 다운로드 사이트](https://downloads.f5.com/esd/index.jsp)에서 버전에 맞는 geoip 데이터 파일 다운로드

- 장비 버전 확인  
![Vue](../manual-images/f5-version.png)

- SCP 를 사용하여 파일 업로드 (/shared/tmp)

- SSH 콘솔 접속 후 아래 명령어 진행

```bash
cd /shared/tmp

mv ip-geolocation-1.0.1-20190225.361.0.zip ./ip-geolocation



cd ip-geolocation/

unzip ip-geolocation-1.0.1-20190225.361.0.zip



geoip_update_data -f geoip-data-Org-1.0.1-20190225.361.0.i686.rpm

geoip_update_data -f geoip-data-Region2-1.0.1-20190225.361.0.i686.rpm

geoip_update_data -f geoip-data-ISP-1.0.1-20190225.361.0.i686.rpm



rm -f geoip-data-*

rm -f README.txt
```
## 주의사항
- 장비 콘솔 접속하여 진행 되는 작업 이므로 명령어 입력에 실수가 없도록 한다.

## 관련항목
- https://downloads.f5.com/esd/index.jsp
- https://support.f5.com/csp/article/K11176 